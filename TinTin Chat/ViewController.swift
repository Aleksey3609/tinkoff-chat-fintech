//
//  ViewController.swift
//  TinTin Chat
//
//  Created by Алексей Горбунов on 23.09.2018.
//  Copyright © 2018 Алексей Горбунов. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func loadView() {
        super.loadView()
         print("Application moved from init() to \(#function)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

         print("Application moved from loadView() to \(#function)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         print("Application moved from viewDidLoad() to \(#function)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         print("Application moved from viewWillAppear() to \(#function)")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
         print("Updating borders  \(#function)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
         print("Application moved from viewWillLayoutSubviews() to \(#function)")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         print("View went off the screen \(#function)")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
         print("Application moved from viewWillDisappear() to \(#function)")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }


}


















